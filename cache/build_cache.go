package main

/* This program is intended to work a as
   background process to generate the resampled
   version of the files exposed by the GSKY WMS
   server. It concurrently calls the resampler
   Python script to produced subsampled versions
   of each file, stored locally with an md5 hashed
   name. */

import (
	"crypto/md5"
	"flag"
	"fmt"
	"log"
	"os/exec"
	"path"
	"path/filepath"
	"sync"
)

func file_gen(destPath, glob string) <-chan string {

	out := make(chan string)

	go func() {
		defer close(out)

		files, err := filepath.Glob(path.Join(destPath, glob))
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range files {
			out <- f
		}
	}()

	return out
}

func file_processor(zoom, destPath string, files <-chan string) {
	for file := range files {
		fmt.Println(file)
		var cmd *exec.Cmd
		cmd = exec.Command("python", "resampler.py", file, zoom, path.Join(destPath, fmt.Sprintf("%x", md5.Sum([]byte(file)))))

		_, err := cmd.Output()
		if err != nil {
			log.Fatal(err)
		}
	}

}

func main() {
	var sFlag = flag.String("src", "/g/data3/fr5/prl900/FC.v302.MCD43A4/", "Source folder to scan files.")
	var dFlag = flag.String("dest", "./", "Destination path for cache.")
	var globFlag = flag.String("glob", "*.nc", "Glob expression.")
	var cFlag = flag.Int("conc", 4, "Concurrency level.")
	var zFlag = flag.String("zoom", ".1", "Resampling zoom level.")
	flag.Parse()

	files := file_gen(*sFlag, *globFlag)
	var wg sync.WaitGroup
	wg.Add(*cFlag)
	for i := 0; i < *cFlag; i++ {
		go func() {
			file_processor(*zFlag, *dFlag, files)
			wg.Done()
		}()
	}
	wg.Wait()
}
