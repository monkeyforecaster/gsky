<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE WMS_DescribeLayerResponse SYSTEM "http://gsky.nci.org.au/schemas/wms/1.1.1/WMS_DescribeLayerResponse.dtd">
<WMS_DescribeLayerResponse version="1.1.1">
	<LayerDescription name="{{ .Name }}" owsURL="http://gsky.nci.org.au/WCS?" owsType="WCS">
		<Query typeName="{{ .Name }}"/>
	</LayerDescription>
</WMS_DescribeLayerResponse>
