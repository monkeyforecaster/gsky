<?xml version="1.0" encoding="UTF-8"?><WMS_Capabilities version="1.3.0" updateSequence="312" xmlns="http://www.opengis.net/wms" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/wms http://qld.auscover.org.au:80/geoserver-dev/schemas/wms/1.3.0/capabilities_1_3_0.xsd">
	<Service>
		<Name>WMS</Name>
		<Title>GSKY Web Map Service</Title>
		<Abstract>An NCI compliant implementation of WMS.</Abstract>
		<KeywordList>
			<Keyword>WFS</Keyword>
			<Keyword>WMS</Keyword>
			<Keyword>GSKY</Keyword>
		</KeywordList>
		<OnlineResource xlink:type="simple" xlink:href="http://gsky.nci.org.au"/>
		<ContactInformation>
			<ContactPersonPrimary>
				<ContactPerson>Pablo Rozas Larraondo</ContactPerson>
				<ContactOrganization>NCI</ContactOrganization>
			</ContactPersonPrimary>
			<ContactPosition>NCI Data Analyst</ContactPosition>
			<ContactAddress>
				<Address>Building 143, Corner of Ward Road and Garran Road Ward Road</Address>
				<City>Acton</City>
				<StateOrProvince>ACT</StateOrProvince>
				<Country>Australia</Country>
			</ContactAddress>
			<ContactElectronicMailAddress>pablo.larraondo@nci.org.au</ContactElectronicMailAddress>
		</ContactInformation>
		<Fees>NONE</Fees>
		<AccessConstraints>NONE</AccessConstraints>
	</Service>
	<Capability>
		<Request>
			<GetCapabilities>
				<Format>text/xml</Format>
				<DCPType>
				  <HTTP>
				    <Get>
				      <OnlineResource xlink:type="simple" xlink:href="http://qld.auscover.org.au:80/geoserver-dev/ows?SERVICE=WMS&amp;"/>
				    </Get>
				    <Post>
				      <OnlineResource xlink:type="simple" xlink:href="http://qld.auscover.org.au:80/geoserver-dev/ows?SERVICE=WMS&amp;"/>
				    </Post>
				  </HTTP>
				</DCPType>
			</GetCapabilities>
			<GetMap>
				<Format>image/png</Format>
				<Format>image/gif</Format>
				<Format>image/jpeg</Format>
				<DCPType>
				  <HTTP>
				    <Get>
				      <OnlineResource xlink:type="simple" xlink:href="http://qld.auscover.org.au:80/geoserver-dev/ows?SERVICE=WMS&amp;"/>
				    </Get>
				  </HTTP>
				</DCPType>
			</GetMap>
			<GetFeatureInfo>
				<Format>text/plain</Format>
				<Format>application/vnd.ogc.gml</Format>
				<Format>text/xml</Format>
				<Format>application/vnd.ogc.gml/3.1.1</Format>
				<Format>text/xml; subtype=gml/3.1.1</Format>
				<Format>text/html</Format>
				<Format>application/json</Format>
				<DCPType>
				  <HTTP>
				    <Get>
				      <OnlineResource xlink:type="simple" xlink:href="http://qld.auscover.org.au:80/geoserver-dev/ows?SERVICE=WMS&amp;"/>
				    </Get>
				  </HTTP>
				</DCPType>
			</GetFeatureInfo>
		</Request>
		<Exception>
			<Format>XML</Format>
			<Format>INIMAGE</Format>
			<Format>BLANK</Format>
			<Format>JSON</Format>
		</Exception>
		<Layer>
			<Title>GSKY Web Map Service</Title>
			<Abstract>A compliant implementation of WMS</Abstract>
			<!--All supported EPSG projections:-->
			<CRS>EPSG:WGS84(DD)</CRS>
			<CRS>EPSG:3857</CRS>
			<CRS>CRS:84</CRS>
			<EX_GeographicBoundingBox>
				<westBoundLongitude>-180.0</westBoundLongitude>
				<eastBoundLongitude>180.0</eastBoundLongitude>
				<southBoundLatitude>-90.0</southBoundLatitude>
				<northBoundLatitude>90.0</northBoundLatitude>
			</EX_GeographicBoundingBox>
			<BoundingBox CRS="CRS:84" minx="-180.0" miny="-90.0" maxx="180.0" maxy="90.0"/>
			{{ range $index, $value := . }}
			<Layer queryable="1" opaque="0">
				<Name>{{ .Name }}</Name>
				<Title>{{ .Title }}</Title>
				<Abstract>{{ .Abstract }}</Abstract>
				<KeywordList>
					<Keyword>WCS</Keyword>
					<Keyword>ImageMosaic</Keyword>
					<Keyword>Fractional Cover</Keyword>
				</KeywordList>
				<CRS>EPSG:4326</CRS>
				<CRS>CRS:84</CRS>
				<EX_GeographicBoundingBox>
					<westBoundLongitude>-180.0</westBoundLongitude>
					<eastBoundLongitude>180.0</eastBoundLongitude>
					<southBoundLatitude>-90.0</southBoundLatitude>
					<northBoundLatitude>90.0</northBoundLatitude>
				</EX_GeographicBoundingBox>
				<BoundingBox CRS="CRS:84" minx="-180.0" miny="-90.0" maxx="180.0" maxy="90.0"/>
				<BoundingBox CRS="EPSG:4326" minx="-90.0" miny="-180.0" maxx="90.0" maxy="180.0"/>
				<Dimension name="time" default="current" units="ISO8601">{{ range $index, $value := .Dates }}{{if $index}},{{end}}{{ $value }}{{ end }}</Dimension>
				<MetadataURL type="ISO19115:2003">
					<Format>text/plain</Format>
					<OnlineResource xlink:type="simple" xlink:href="http://data.auscover.org.au/geonetwork?uuid=d7369c11-3235-40d0-8060-6af6ddb339a7"/>
				</MetadataURL>
				<DataURL>
					<Format>text/plain</Format>
					<OnlineResource xlink:type="simple" xlink:href="http://www.auscover.org.au/purl/fractional-cover-modis-csiro"/>
				</DataURL>
				<Style>
					<Name>{{ .Name }}</Name>
					<Title>{{ .Title }}</Title>
					<Abstract>A sample style that draws a raster, good for displaying imagery</Abstract>
					<LegendURL width="160" height="424">
						<Format>image/png</Format>
						<OnlineResource xlink:type="simple" xlink:href="http://130.56.242.20/WMS?service=WMS&amp;request=GetLegendGraphic&amp;version=1.3.0&amp;layers={{ .Name }}"/>
					</LegendURL>	
				</Style>
			</Layer>
			{{end}}
		</Layer>
	</Capability>
</WMS_Capabilities>
