package utils

import (
	"encoding/json"
	"fmt"
	"image/color"
	"io/ioutil"
	"time"
)
// DataSource contains the source files of one layer
// as well as the max and min resolutions it represents.
// Its fields encode the path to the collection and
// the maximum and mimum resolution that can be served.
// This enables the definition of different versions
// of the same dataset with different resolutions
// to speedup response times.
type DataSource struct {
	Path   string  `json:"path"`
	MinRes float64 `json:"min_res"`
	MaxRes float64 `json:"max_res"`
}

// Layer contains all the details that a layer needs
// to be published and rendered
type Layer struct {
	Name         string       `json:"name"`
	Title        string       `json:"title"`
	Abstract     string       `json:"abstract"`
	DataSources  []DataSource `json:"data_sources"`
	StartISODate string       `json:"start_isodate"`
	EndISODate   string       `json:"end_isodate"`
	StepDays     int          `json:"step_days"`
	Dates        []string     `json:"dates"`
	RGBProducts  []string     `json:"rgb_products"`
	Palette      []color.RGBA `json:"palette"`
	LegendPath   string       `json:"legend_path"`
}

// Config is the struct representing the configuration
// of a WMS server. It contains information about the
// file index API as well as the list of WMS layers that
// can be served.
type Config struct {
	ApiAddress string  `json:"api_address"`
	Layers     []Layer `json:"layers"`
}

// string used to format Go ISO times
const ISOFormat = "2006-01-02T15:04:05.000Z"

// GenerateDatesMCD43A4 function is used to generate the
// list of ISO dates from its especification in the
// Config.Layer struct. This is a Modis specific function.
func GenerateDatesMCD43A4(start, end time.Time, step time.Duration) []string {

	dates := []string{}

	year := start.Year()
	for start.Before(end) {
		for start.Year() == year && start.Before(end) {
			dates = append(dates, start.Format(ISOFormat))
			start = start.Add(time.Hour * step)
		}
		if start.After(end) {
			break
		}
		year = start.Year()
		start = time.Date(year, 1, 1, 0, 0, 0, 0, time.UTC)
	}

	return dates
}

// GetConfig marshall the config.json document
// returning an instance of a Config variable
// containing all the values
func GetConfig(configFile string) (Config, error) {
	cfg, err := ioutil.ReadFile(configFile)
	if err != nil {
		return Config{}, fmt.Errorf("Error while reading config file: %s. Error: %v", configFile, err)
	}

	var config Config
	err = json.Unmarshal(cfg, &config)
	if err != nil {
		return Config{}, fmt.Errorf("Error at JSON parsing config document: %s. Error: %v", configFile, err)
	}

	for i, layer := range config.Layers {
		start, _ := time.Parse(ISOFormat, layer.StartISODate)
		end, _ := time.Parse(ISOFormat, layer.EndISODate)
		config.Layers[i].Dates = GenerateDatesMCD43A4(start, end, time.Duration(24*layer.StepDays))
		if layer.Palette != nil && len(layer.Palette) < 3 {
			return Config{}, fmt.Errorf("The list of colors must have at least 2 colors.")
		}
	}

	return config, nil
}
