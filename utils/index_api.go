package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// FileList is the struct used to unmarshal
// the reponse from the index API
type FileList struct {
	Tiles []string `json:"tiles"`
}

// QueryIndexAPI makes a request to the index API
// returning the result as an instance of the
// FileList type.
func QueryIndexAPI(apiAddress, path, time, srs string, bbox []float64) (FileList, error) {
	url := fmt.Sprintf("http://%s%s?wms&time=%s&srs=%s&bbox=%f,%f,%f,%f", apiAddress, path, time, srs, bbox[0], bbox[1], bbox[2], bbox[3])
	resp, err := http.Get(url)
	if err != nil {
		return FileList{}, fmt.Errorf("GET request to  %s failed. Error: %v", url, err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return FileList{}, fmt.Errorf("Error parsing response body from %s. Error: %v", url, err)
	}

	var tiles FileList
	err = json.Unmarshal(body, &tiles)
	if err != nil {
		return FileList{}, fmt.Errorf("Problem parsing JSON response from %s. Error: %v", url, err)
		return tiles, err
	}
	return tiles, nil
}
