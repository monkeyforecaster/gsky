package utils

// #include "gdal.h"
// #include "cpl_string.h"
// #cgo LDFLAGS: -lgdal
import "C"

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"
)

// GetNCTime returns the index corresponding
// to a unixDate inside the especified netCDF
// file and dataset. It's implemented wrapping the
// gdal C library but can be also done wrapping the
// netCDF4 C library.
func GetNCTime(sdsName string, unixDate int64) (int, error) {
	nameParts := strings.Split(sdsName, ":")
	times := []int64{}

	if len(nameParts) != 3 || nameParts[0] != "NETCDF" {
		return -1, fmt.Errorf("%s is not a valid GDAL dataset", sdsName)
	}

	C.GDALAllRegister()
	hSubdataset := C.GDALOpen(C.CString(sdsName), C.GA_ReadOnly)
	defer C.GDALClose(hSubdataset)

	metadata := C.GDALGetMetadata(hSubdataset, C.CString(""))
	value := C.CSLFetchNameValue(metadata, C.CString("NETCDF_DIM_time_VALUES"))
	if value == nil {
		return -1, fmt.Errorf("%s does not contain the \"NETCDF_DIM_time_VALUES\" variable", sdsName)
	}

	timeStr := C.GoString(value)
	for _, tStr := range strings.Split(strings.Trim(timeStr, "{}"), ",") {
		tF, _ := strconv.ParseFloat(tStr, 64)
		secs, _ := math.Modf(tF)
		t := time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)
		t = t.Add(time.Second * time.Duration(secs))
		times = append(times, t.Unix())
	}

	for i, time := range times {
		if time == unixDate {
			return i + 1, nil
		}
	}

	return -1, fmt.Errorf("%s dataset does not contain Unix date: %d", sdsName, unixDate)
}
