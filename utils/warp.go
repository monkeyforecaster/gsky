package utils

// #include "gdal.h"
// #include "gdal_alg.h"
// #include "ogr_api.h"
// #include "ogr_srs_api.h"
// #include "cpl_string.h"
// #cgo LDFLAGS: -lgdal
// int
// warp_caller(GDALDatasetH hSrcDS, GDALDatasetH hDstDS, int nBandCount, int *panBandList)
// {
//        void *pTransformArg;
//        pTransformArg = GDALCreateGenImgProjTransformer(hSrcDS, GDALGetProjectionRef(hSrcDS), hDstDS, GDALGetProjectionRef(hDstDS), 0, 0, 0);
//        return GDALSimpleImageWarp(hSrcDS, hDstDS, 1, panBandList, GDALGenImgProjTransform, pTransformArg, NULL, NULL, NULL);
// }
import "C"

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"sync"
	"time"
	"unsafe"
)

// WarpParams is the object received by
// the warp server to render a tile.
type WarpParams struct {
	Prod        string       `json:"prod"`
	Geot        []float64    `json:"geot"`
	SRS         string       `json:"srs"`
	Width       int          `json:"width"`
	Height      int          `json:"height"`
	Tiles       []string     `json:"tiles"`
	Time        string       `json:"time"`
	RGBProducts []string     `json:"rgb_products"`
	Palette     []color.RGBA `json:"palette"`
}

// EPSG2WKT maps EPSG codes to WKT projections.
var EPSG2WKT map[string]string = map[string]string{"EPSG:3857": `PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],PROJECTION["Mercator_1SP"],PARAMETER["central_meridian",0],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["X",EAST],AXIS["Y",NORTH],EXTENSION["PROJ4","+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs"],AUTHORITY["EPSG","3857"]]`,
	"EPSG:4326": `GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]`}

// BBox2Geot return the geotransform from the
// parameters received in a WMS GetMap request
func BBox2Geot(width, height int, bbox []float64) []float64 {
	return []float64{bbox[0], (bbox[2] - bbox[0]) / float64(width), 0, bbox[3], 0, (bbox[1] - bbox[3]) / float64(height)}
}

// InterpolateUint8 interpolates the value of a
// byte between two numbers 'a' and 'b' by
// especifying a length and a position 'i'
// along that length.
func InterpolateUint8(a, b uint8, i, sectionLength int) uint8 {
	return a + uint8((i * (int(b) - int(a)) / sectionLength))
}

// InterpolateColor returns an RGBA color where
// the R, G, B, and A components have been
// interpolated from the 'a' and 'b' colors
func InterpolateColor(a, b color.RGBA, i, sectionLength int) color.RGBA {
	return color.RGBA{InterpolateUint8(a.R, b.R, i, sectionLength),
		InterpolateUint8(a.G, b.G, i, sectionLength),
		InterpolateUint8(a.B, b.B, i, sectionLength),
		255}
}

// GradientPalette returns a palette of 256 colors
// creating an interpolation that goes though
// a list of provided colours.
func GradientRGBAPalette(colors []color.RGBA) ([]color.RGBA, error) {
	palette := make([]color.RGBA, 256)

	sectionLength := 256 / (len(colors) - 1)

	for section := 0; section < len(colors)-1; section++ {
		for i := 0; i < sectionLength; i++ {
			palette[section*sectionLength+i] = InterpolateColor(colors[section], colors[section+1], i, sectionLength)
		}
	}

	return palette, nil
}

// ColorPaletteTile generates a png tile
// using a colored palette. This function
// heavily relies on GDAL to read and warp
// the list of images provided. rgb_prods
// contains the layer and the rest of the
// parameters contains details to reder the
// tile.
func ColorPaletteTile(files, rgb_prods []string, tileDateStr string, xSize, ySize int, srs string, geot []float64, colors []color.RGBA) ([]byte, error) {
	canvas := image.NewGray(image.Rect(0, 0, xSize, ySize))
	mask := image.NewGray(image.Rect(0, 0, xSize, ySize))
	temp := make([]uint8, xSize*ySize)

	tileDate, err := time.Parse(ISOFormat, tileDateStr)
	if err != nil {
		return nil, fmt.Errorf("Error converting ISO date string %s: %v", tileDateStr, err)
	}

	var wg sync.WaitGroup

	for _, file := range files {
		index, err := GetNCTime(fmt.Sprintf("NETCDF:%s:%s", file, rgb_prods[0]), tileDate.Unix())
		if err != nil {
			return nil, err
		}

		wg.Add(1)
		go ReadLayer(fmt.Sprintf("NETCDF:%s:%s", file, rgb_prods[0]), temp, geot, srs, xSize, ySize, index, &wg)
		wg.Wait()

		for i := 0; i < len(temp); i++ {
			if temp[i] != 0x00 && temp[i] != 0xff {
				// Scaling specific to this dataset. Remove to be generic
				canvas.Pix[i] = (100 - temp[i]) * 2
				mask.Pix[i] = 255
			}
		}
	}

	palette, err := GradientRGBAPalette(colors)
	if err != nil {
		return nil, err
	}

	dst := image.NewRGBA(canvas.Bounds())

	for x := 0; x < canvas.Bounds().Dx(); x++ {
		for y := 0; y < canvas.Bounds().Dy(); y++ {
			if mask.Pix[x+y*mask.Stride] == 0xff {
				dst.Set(x, y, palette[int(canvas.Pix[x+y*canvas.Stride])])
			}
		}
	}

	buf := new(bytes.Buffer)
	err = png.Encode(buf, dst)
	if err != nil {
		return nil, fmt.Errorf("Error encoding png tile: %v", err)
	}

	return buf.Bytes(), nil
}

// RGBTile generates a png tile using
// the Red, Green and Blue layers in
// rgb_prods. This function heavily relies
// on GDAL to read and warp the list of
// images provided, The rest of the
// parameters contains details to reder the
// tile.
func RGBTile(files, rgb_prods []string, tileDateStr string, xSize, ySize int, srs string, geot []float64) ([]byte, error) {
	canvas := image.NewRGBA(image.Rect(0, 0, xSize, ySize))
	tempR := make([]uint8, xSize*ySize)
	tempG := make([]uint8, xSize*ySize)
	tempB := make([]uint8, xSize*ySize)

	tileDate, err := time.Parse(ISOFormat, tileDateStr)
	if err != nil {
		return nil, fmt.Errorf("Error converting ISO date string %s: %v", tileDateStr, err)
	}

	var wg sync.WaitGroup

	for _, file := range files {
		index, err := GetNCTime(fmt.Sprintf("NETCDF:%s:%s", file, rgb_prods[0]), tileDate.Unix())
		if err != nil {
			return nil, err
		}

		wg.Add(1)
		go ReadLayer(fmt.Sprintf("NETCDF:%s:%s", file, rgb_prods[0]), tempR, geot, srs, xSize, ySize, index, &wg)
		wg.Add(1)
		go ReadLayer(fmt.Sprintf("NETCDF:%s:%s", file, rgb_prods[1]), tempG, geot, srs, xSize, ySize, index, &wg)
		wg.Add(1)
		go ReadLayer(fmt.Sprintf("NETCDF:%s:%s", file, rgb_prods[2]), tempB, geot, srs, xSize, ySize, index, &wg)
		wg.Wait()

		var start int
		for i := 0; i < len(tempR); i++ {
			if tempR[i] != 0x00 && tempR[i] != 0xff { // || (tempG[i] != 0x00 && tempG[i] != 0xff) || (tempB[i] != 0x00 && tempB[i] != 0xff) {
				// Scaling specific to this dataset. Remove to be generic
				start = i * 4
				canvas.Pix[start] = tempR[i] * 2
				canvas.Pix[start+1] = tempG[i] * 2
				canvas.Pix[start+2] = tempB[i] * 2
				canvas.Pix[start+3] = 0xff
			}
		}
	}

	buf := new(bytes.Buffer)
	err = png.Encode(buf, canvas)
	if err != nil {
		return nil, fmt.Errorf("Error encoding png tile: %v", err)
	}

	return buf.Bytes(), nil
}

// ReadLayer uses GDAL to open and read
// the contents of the especified file
// and band.
func ReadLayer(fileName string, canvas []uint8, geot []float64, srs string, xSize, ySize, band int, wg *sync.WaitGroup) error {
	defer wg.Done()
	C.GDALAllRegister()
	hSrcDS := C.GDALOpen(C.CString(fileName), C.GA_ReadOnly)
	defer C.GDALClose(hSrcDS)

	// Create output file
	hDstDS := C.GDALOpen(C.CString(fmt.Sprintf("MEM:::DATAPOINTER=%d,PIXELS=%d,LINES=%d,DATATYPE=Byte", unsafe.Pointer(&canvas[0]), C.int(xSize), C.int(ySize))), C.GA_Update)
	defer C.GDALClose(hDstDS)

	// Write out the projection definition.
	C.GDALSetProjection(hDstDS, C.CString(EPSG2WKT[srs]))
	C.GDALSetGeoTransform(hDstDS, (*C.double)(&geot[0]))

	panBandList := []C.int{C.int(band)}
	err := C.warp_caller(hSrcDS, hDstDS, 1, &panBandList[0])
	if err != 1 {
		return fmt.Errorf("GDAL Warp error: %s. Error no: %d\n", fileName, err)
	}

	return nil
}
