package main

/* warp is a web server delivering png rendered tiles.
   This server is intended to be consumed by a wms server
   which sends a WarpParams request with the details of
   the tile to be rendered. Most of the heavylifting
   operations are done with calls to the C GDAL library
   and code details are in utils/warp.go */

import (
	"./utils"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	_ "net/http/pprof"
)

var (
	Error *log.Logger
)

// init initialises the Error logger and Config struct.
// This is the first function to be called in main.
func init() {
	Error = log.New(os.Stderr, "WARP: ", log.Ldate|log.Ltime|log.Lshortfile)
}

// WriteErrorCode sends the specified error code and message to the client.
// It also reports the error on the Error log.
// It can be extented with new error codes.
func WriteErrorCode(w http.ResponseWriter, errorCode int, msg string) {
	switch errorCode {
	case 500:
		Error.Printf("%s\n", msg)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("HTTP/1.1 500: Internal Server Error. %s", msg)))
	case 400:
		Error.Printf("%s\n", msg)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("HTTP/1.1 400: Bad Request. %s", msg)))
	}
}

// tileHandler handles every request received on /tiler
func tileHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		var warpParams utils.WarpParams
		err := decoder.Decode(&warpParams)
		if err != nil {
			w.Write([]byte(fmt.Sprintf("HTTP/1.1 400 Bad Request: Malformed request.")))
			return
		}

		if len(warpParams.RGBProducts) == 1 {
			b, err := utils.ColorPaletteTile(warpParams.Tiles, warpParams.RGBProducts, warpParams.Time, warpParams.Width, warpParams.Height, warpParams.SRS, warpParams.Geot, warpParams.Palette)
			if err != nil {
				WriteErrorCode(w, 500, fmt.Sprintf("Error generating Paletted tile: %v.", err))
				return
			}
			w.Write(b)
		} else if len(warpParams.RGBProducts) == 3 {
			b, err := utils.RGBTile(warpParams.Tiles, warpParams.RGBProducts, warpParams.Time, warpParams.Width, warpParams.Height, warpParams.SRS, warpParams.Geot)
			if err != nil {
				WriteErrorCode(w, 500, fmt.Sprintf("Error generating RGB tile: %v.", err))
				return
			}
			w.Write(b)
		} else {
			WriteErrorCode(w, 400, fmt.Sprintf("Unexpected %d length of 'rgb_products' field in posted JSON document.", len(warpParams.RGBProducts)))
		}
	} else {
		WriteErrorCode(w, 400, fmt.Sprintf("Only POST requests are accepted."))
	}
}

func main() {
	http.HandleFunc("/tiler", tileHandler)
	http.ListenAndServe("0.0.0.0:6000", nil)
}
