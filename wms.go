package main

/* wms is a web server implementing the WMS protocol
   to serve maps. This server is intended to be consumed
   directly by users and exposes a series of functionalities
   through the GetCapabilities.xml document.
   Configuration of the server is specified at the config.json
   file were features such as layers or color scales can be 
   defined.
   This server depends on two other services to operate: the 
   index server which registers the files involved in one operation
   and the warp server which performs the actual rendering of 
   a tile.
   Most of the functionality for this service is contained in the
   utils/wms.go file */

import (
	"./utils"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"net/http"
	"os"
	_ "net/http/pprof"
)

// Global variable to hold the values especified
// on the config.json document.
var config utils.Config

var reMap map[string]*regexp.Regexp

// WriteErrorCode sends the specified error code and message to the client.
var (
	Error *log.Logger
	Info  *log.Logger
)

// init initialises the Error logger, checks
// required files are in place  and sets Config struct.
// This is the first function to be called in main.
func init() {
	Error = log.New(os.Stderr, "WMS: ", log.Ldate|log.Ltime|log.Lshortfile)
	Info = log.New(os.Stdout, "WMS: ", log.Ldate|log.Ltime|log.Lshortfile)

	filePaths := []string{"./config.json", "./templates/WMS_GetCapabilities.tpl",
		              "./templates/WMS_DescribeLayer.tpl", "./templates/WMS_ServiceException.tpl"}

	for _, filePath := range filePaths {
		if _, err := os.Stat(filePath); os.IsNotExist(err) {
			panic(err)
		}
	}

	var err error
	config, err = utils.GetConfig("./config.json")
	if err != nil {
		Error.Printf("%v\n", err)
		panic(err)
	}

	reMap = utils.CompileRegexMap()
}

// WriteErrorCode sends the specified error code and message to the client.
// It also reports the error on the Error log.
// It can be extented with new error codes.
func WriteErrorCode(w http.ResponseWriter, errorCode int, msg string) {
	switch errorCode {
	case 500:
		Error.Printf("%s\n", msg)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("HTTP/1.1 500: Internal Server Error. %s", msg)))
	case 400:
		Error.Printf("%s\n", msg)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("HTTP/1.1 400: Bad Request. %s", msg)))
	}
}

// mapHandler handles every request received on /WMS
func mapHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	Info.Printf("%s\n", r.URL.String())

	query := r.URL.Query()
	if _, fOK := query["service"]; !fOK || query["service"][0] != "WMS" {
		WriteErrorCode(w, 400, fmt.Sprintf("Not a WMS request. URL %s does not contain a 'request=WMS' parameter.", r.URL.String()))
		return
	}

	params, err := utils.WMSParamsChecker(r.URL.Query(), reMap)
	if err != nil {
		WriteErrorCode(w, 400, fmt.Sprintf("Wrong WMS parameters on URL: %s", err))
		return
	}

	switch *params.Request {
	case "GetCapabilities":
		err := utils.ExecuteWriteTemplateFile(w, config.Layers, "./templates/WMS_GetCapabilities.tpl")
		if err != nil {
			WriteErrorCode(w, 500, fmt.Sprintf("%s", err))
		}
	case "GetFeatureInfo":
		x, y := utils.GetCoordinates(params)
		resp := fmt.Sprintf(`{"type":"FeatureCollection","totalFeatures":"unknown","features":[{"type":"Feature","id":"","geometry":null,"properties":{"x":%f, "y":%f}}],"crs":null}`, x, y)
		w.Write([]byte(resp))

	case "DescribeLayer":
		prod, err := utils.GetProduct(params, config)
		if err != nil {
			Error.Printf("%s\n", err)
			//utils.WriteException(config, prod, w)
			utils.ExecuteWriteTemplateFile(w, prod, "./templates/WMS_ServiceException.tpl")
			return
		}

		idx, err := utils.GetLayerIndex(params, prod, config)
		if err != nil {
			Error.Printf("%s\n", err)
			//utils.WriteException(config, prod, w)
			utils.ExecuteWriteTemplateFile(w, prod, "./templates/WMS_ServiceException.tpl")
			return
		}

		//err = utils.WriteDescribeLayer(config, idx, w)
		err = utils.ExecuteWriteTemplateFile(w, config.Layers[idx], "./templates/WMS_DescribeLayer.tpl")
		if err != nil {
			WriteErrorCode(w, 500, fmt.Sprintf("%s", err))
		}

	case "GetMap":

		geot := utils.BBox2Geot(*params.Width, *params.Height, params.BBox)

		prod, err := utils.GetProduct(params, config)
		if err != nil {
			Error.Printf("%s\n", err)
			//utils.WriteException(config, prod, w)
			utils.ExecuteWriteTemplateFile(w, prod, "./templates/WMS_ServiceException.tpl")
			return
		}

		idx, err := utils.GetLayerIndex(params, prod, config)
		if err != nil {
			Error.Printf("%s\n", err)
			//utils.WriteException(config, prod, w)
			utils.ExecuteWriteTemplateFile(w, prod, "./templates/WMS_ServiceException.tpl")
			return
		}

		path := ""
		for _, dataSource := range config.Layers[idx].DataSources {
			if dataSource.MinRes <= geot[1] && geot[1] < dataSource.MaxRes {
				path = dataSource.Path
				break
			}
		}
		if path == "" {
			WriteErrorCode(w, 400, fmt.Sprintf("Source dataset not identified. There might be a problem in the configuration file"))
			return
		}
		if params.Time == nil {
			WriteErrorCode(w, 400, fmt.Sprintf("Request %s should contain a valid ISO 'time' parameter.", r.URL.String()))
			return
		}
		if params.CRS == nil {
			WriteErrorCode(w, 400, fmt.Sprintf("Request %s should contain a valid 'crs'/'srs' parameter.", r.URL.String()))
			return
		}
		tiles, err := utils.QueryIndexAPI(config.ApiAddress, path, params.Time.Format(utils.ISOFormat), *params.CRS, params.BBox)
		if err != nil {
			WriteErrorCode(w, 500, fmt.Sprintf("%s", err))
			return
		}

		warpParams := utils.WarpParams{Prod: prod, Geot: geot, SRS: *params.CRS, Width: *params.Width, Height: *params.Height, Tiles: tiles.Tiles, Time: params.Time.Format(utils.ISOFormat), RGBProducts: config.Layers[idx].RGBProducts, Palette: config.Layers[idx].Palette}

		buf := new(bytes.Buffer)
		json.NewEncoder(buf).Encode(warpParams)
		resp, err := http.Post("http://0.0.0.0:6000/tiler", "application/json", buf)
		if err != nil {
			WriteErrorCode(w, 500, fmt.Sprintf("%s", err))
			return
		}

		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			WriteErrorCode(w, 500, fmt.Sprintf("%s", err))
			return
		}

		w.Write(body)

	case "GetLegendGraphic":
		prod, err := utils.GetProduct(params, config)
		if err != nil {
			WriteErrorCode(w, 500, fmt.Sprintf("%s", err))
			return
		}

		idx, err := utils.GetLayerIndex(params, prod, config)
		if err != nil {
			WriteErrorCode(w, 500, fmt.Sprintf("%s", err))
			return
		}

		b, err := ioutil.ReadFile(config.Layers[idx].LegendPath)
		if err != nil {
			WriteErrorCode(w, 500, fmt.Sprintf("%s", err))
			return
		}
		w.Write(b)

	default:
		WriteErrorCode(w, 400, fmt.Sprintf("%s not recognised.", *params.Request))
	}
}

func main() {
	fs := http.FileServer(http.Dir("static"))
	http.Handle("/", fs)
	http.HandleFunc("/WMS", mapHandler)
	http.ListenAndServe("0.0.0.0:8080", nil)
}
